<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

    protected function setUp()
    {
      $this->Poneys = new Poneys();
      $this->Poneys->setCount(8);
    }


    protected function tearDown()
    { // detruire la variable Poneys
      unset($this->Poneys);
    }
    public function test_removePoneyFromField() {
      // Setup
      

      // Action
      $this->Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->Poneys->getCount());
    }

    public function test_addPoneyIntoField() {
 
      // Action
      $this->Poneys->addPoneyIntoField(4);
      
      // Assert
      $this->assertEquals(12, $this->Poneys->getCount());
    }

    /**
     * @expectedException Exception
     */
    public function testException_removePoneyFromField()
    {

      // Action
      $this->Poneys->removePoneyFromField(15);
    }

     /**
     * @dataProvider removePoneyFromFieldProvider
     */
    public function testDataProvider_removePoneyFromField($number, $expected)
    {
      // Action
      $this->Poneys->removePoneyFromField($number);
      $this->assertEquals($this->Poneys->getCount(), $expected);
    }

    public function removePoneyFromFieldProvider()
    {
        return [
            [1, 7],
            [6, 2],
            [0, 8],
            [8, 0]
        ];
    }

   public function testMock()
      {
          // Create a stub for the Poneys class.
          $mock = $this->getMockBuilder(Poneys::class)->setMethods(['getNames'])->getMock();
  
          // Configure the stub.
         $mock->expects($this->once())
                 ->method('getNames')
                 ->willReturn(['toto', 'titi', 'tata']);

          $this->assertEquals(['toto', 'titi', 'tata'], $mock->getNames());
      }

    public function testCanIaddPoney(){
      // Action
       $this->assertTrue($this->Poneys->ifCanIaddPoney());
      $this->Poneys->addPoneyIntoField(7);
        $this->assertFalse($this->Poneys->ifCanIaddPoney());
    }
  }
 ?>
