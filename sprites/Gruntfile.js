module.exports = function (grunt) {
  // Configure grunt
  grunt.initConfig({
    sprite:{
      all: {
        src: 'img/*.png',
        dest: 'img/spritesheet.png',
        destCss: 'css/sprites.css'
      }
    }
  });
  grunt.loadNpmTasks('grunt-spritesmith');

};